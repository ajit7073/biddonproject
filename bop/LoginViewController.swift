//
//  LoginViewController.swift
//  bop
//
//  Created by apple on 13/02/19.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {
    
    @IBOutlet weak var registrationButton: UIButton!
    @IBOutlet weak var userNameLoginTextField: UITextField!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var loginPasswordTextField: UITextField!
    var userLoginWiseArr = [[String:Any]]()
    override func viewDidLoad() {
        super.viewDidLoad()
        loginButton.layer.cornerRadius = 10
        registrationButton.layer.cornerRadius = 10
        
        // Do any additional setup after loading the view.
    }
    
    
    
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
        
        if segue.identifier == "showSignUP" {
            let destination = segue.destination as! RegistrationViewController
            destination.setAlertDelegate = self
        }
     }
    
    
    @IBAction func signupButtonAction(_ sender: Any) {
        self.performSegue(withIdentifier: "showSignUP", sender: nil)
    }
    @IBAction func loginButtonAction(_ sender: Any) {
        
        chackValiddation()
        
    }
    
    
    func chackValiddation() {
        if userNameLoginTextField.text?.count ?? 0 > 0 {
            if validateEmail(enteredEmail: userNameLoginTextField.text ?? ""){
                
            }else {
                alertMessage(message: "Please valid email Address.")
                return
            }
        }else {
            alertMessage(message: " Required email address!")
            return
        }
        if  isValidated(loginPasswordTextField.text ?? "") {
            
            
        }else {
            alertMessage(message: " Required password should be 8 character or must be strong.")
            return
        }
        postLoginCall()
        
    }
    func validateEmail(enteredEmail:String) -> Bool {
        let emailFormat = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailPredicate = NSPredicate(format:"SELF MATCHES %@", emailFormat)
        let email = emailPredicate.evaluate(with: enteredEmail)
        
        return email
    }
    
    func isValidated(_ password: String) -> Bool {
        var lowerCaseLetter: Bool = false
        var upperCaseLetter: Bool = false
        var digit: Bool = false
        var specialCharacter: Bool = false
        
        if password.characters.count  >= 8 {
            for char in password.unicodeScalars {
                if !lowerCaseLetter {
                    lowerCaseLetter = CharacterSet.lowercaseLetters.contains(char)
                }
                if !upperCaseLetter {
                    upperCaseLetter = CharacterSet.uppercaseLetters.contains(char)
                }
                if !digit {
                    digit = CharacterSet.decimalDigits.contains(char)
                }
                if !specialCharacter {
                    specialCharacter = CharacterSet.punctuationCharacters.contains(char)
                }
            }
            if specialCharacter || (digit && lowerCaseLetter && upperCaseLetter) {
                //do what u want
                return true
            }
            else {
                return false
            }
        }
        return false
    }
    
    
    func alertMessage(message:String) {
        let alert = UIAlertController(title: "", message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "ok", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    //MARK:- API
    
    func postLoginCall(){
        let request = NSMutableURLRequest(url: NSURL(string: "\(baseUrl)userlogin.php")! as URL)
        request.httpMethod = "POST"
        let postString = "email=\(userNameLoginTextField.text!)&pass=\(loginPasswordTextField.text!)"
        print(postString)
        request.setValue("application/x-www-form-urlencoded; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.httpBody = postString.data(using: String.Encoding.utf8)
        
        let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
            guard error == nil && data != nil else {                                                          // check for fundamental networking error
                print("error=\(String(describing: error))")
                return
            }
            
            do {
                if let responseJSON = try JSONSerialization.jsonObject(with: data!) as? [String:AnyObject]{
                    print(responseJSON)
                    print(responseJSON["success"]!)
                   
                    
                    //Check response from the sever
                    let responds = responseJSON["success"]! as! Int
                    DispatchQueue.main.async {
                        
                        if responds == 1 {
                            self.userLoginWiseArr = responseJSON["userlogin"] as! [[String : Any]]
                            UserDefaults.standard.set(self.userLoginWiseArr[0]["i_want_to"], forKey: "i_want_to")
                            UserDefaults.standard.set(true, forKey: "isLogin")
                            UserDefaults.standard.set(self.userLoginWiseArr[0]["username"], forKey: "username")
                            
                            self.performSegue(withIdentifier: "segueTabbar", sender: nil)
                            self.alertMessage(message: "Login successfull!")
                        }else {
                            self.alertMessage(message:"Please valid Email Or Password!")
                        }
                    }
                }
            }
            catch {
                print("Error -> \(error)")
            }
        }
        task.resume()
    }
}

extension LoginViewController:RegistrationViewControllerDelegate {
    func openAlert(title: String) {
        alertMessage(message: title)
    }
    
    
}
