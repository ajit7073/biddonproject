//
//  workerListTableViewCell.swift
//  bop
//
//  Created by apple on 12/04/19.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit
import AARatingBar

class workerListTableViewCell: UITableViewCell {
    @IBOutlet weak var workerImageView: UIImageView!
    
    @IBOutlet weak var priceLable: UILabel!
    @IBOutlet weak var reviewLabel: UILabel!
    @IBOutlet weak var countryNameLabel: UILabel!
    
    @IBOutlet weak var rattingBarView: AARatingBar!
    @IBOutlet weak var countryImageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
