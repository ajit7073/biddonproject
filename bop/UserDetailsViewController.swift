//
//  UserDetailsViewController.swift
//  bop
//
//  Created by apple on 13/04/19.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit
import SDWebImage
import SVProgressHUD

protocol UserDetailsViewControllerDelegate {
    func setalertMessage()
}

class UserDetailsViewController: UIViewController {
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var mySkills2Label: UILabel!
    
    @IBOutlet weak var userReviewTableview: UITableView!
    @IBOutlet weak var hireMeButton: UIButton!
    @IBOutlet weak var mySkills3Label: UILabel!
    @IBOutlet weak var mySkills1Label: UILabel!
    @IBOutlet weak var aboutDetailsLabel: UILabel!
    @IBOutlet weak var profileReviewsSegmentControl: UISegmentedControl!
    @IBOutlet weak var userNameLabel: UILabel!
    var setAlertDelegate : UserDetailsViewControllerDelegate?
    var reviewArray = [[String:Any]]()
    
    var userDetails = [String:Any]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tabBarController?.tabBar.isHidden = true
        userImageView.layer.cornerRadius = userImageView.frame.size.width / 2
        userNameLabel.text = userDetails["Worker_Name"] as? String ?? ""
         let url = URL(string: userDetails["Worker_Image"] as? String ?? "")
        self.userImageView?.sd_setImage(with: url, placeholderImage:nil, completed: { (image, error, cacheType, url) -> Void in
            if (error == nil) {
                // set the placeholder image here
                
            } else {
                self.userImageView.image = image
                // success ... use the image
            }
        })
        aboutDetailsLabel.text = userDetails["About"] as? String ?? ""
        mySkills1Label.text = "WordPress"
        mySkills2Label.text = "PHP"
        mySkills3Label.text = "Website Design"
        userReviewTableview.delegate = self
        userReviewTableview.dataSource = self
        getRatting(urlApi: "")
        

        // Do any additional setup after loading the view.
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        tabBarController?.tabBar.isHidden = false

    }
    
    @IBAction func profileReviewsSegmentControlAction(_ sender: Any) {
        if profileReviewsSegmentControl.selectedSegmentIndex == 1 {
            userReviewTableview.isHidden = false
        }else {
            userReviewTableview.isHidden = true
        }
    }
    @IBAction func hireMeButtonAction(_ sender: Any) {
         self.navigationController?.popViewController(animated: true)
        setAlertDelegate?.setalertMessage()
       
    }
    
    func alertMessage(message:String) {
        let alert = UIAlertController(title: "", message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "ok", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    //MARK:- API
    func getRatting(urlApi :String) {
        
        SVProgressHUD.show()
        let request = NSMutableURLRequest(url: NSURL(string:"\(baseUrl)select_review.php")! as URL)
        request.httpMethod = "GET"
       
        let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
            guard error == nil && data != nil else {                                                          // check for fundamental networking error
                print("error=\(String(describing: error))")
                return
            }
            
            do {
                if let responseJSON = try JSONSerialization.jsonObject(with: data!) as? [String:AnyObject]{
                    print(responseJSON)
                    print(responseJSON["success"]!)
                    DispatchQueue.main.async {
                        self.reviewArray = responseJSON["review_details"] as! [[String : Any]]
                        self.userReviewTableview.reloadData()
                        SVProgressHUD.dismiss()
                    }
                }
                
            }
            catch {
                print("Error -> \(error)")
                SVProgressHUD.dismiss()
            }
            
            
            
        }
        
        
        task.resume()
        
        
        
    }
    

}

extension UserDetailsViewController :UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return reviewArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let identifier = "userReviewTableViewCell"
        let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as!  userReviewTableViewCell
        let values = reviewArray[indexPath.row]
        cell.titleReviewLabel.text = values["Review"] as? String ?? ""
        cell.ratingLabel.text = values["Rating"] as? String ?? ""
        let value1 = CGFloat(Double(values["Rating"] as? String ?? "0.0")!)
        cell.starratingView.value =  value1
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let date = dateFormatter.date(from: values["Entry_Date"] as? String ?? "")
        

        cell.timeLabel.text =  timeAgoSinceDate(date!, currentDate: Date(), numericDates: true)
        /*
        cell.reviewerNameLabel.text = values["name"] as? String ?? ""
        
        let url = URL(string: values["image"] as? String ?? "")
        cell.reviewerUserImageView.sd_setImage(with: url, placeholderImage:nil, completed: { (image, error, cacheType, url) -> Void in
            if (error == nil) {
                // set the placeholder image here
                
            } else {
                cell.reviewerUserImageView.image = image
                // success ... use the image
            }
        })
        */
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    
}
