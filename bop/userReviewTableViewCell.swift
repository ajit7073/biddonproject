//
//  userReviewTableViewCell.swift
//  bop
//
//  Created by apple on 15/04/19.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit
import AARatingBar

class userReviewTableViewCell: UITableViewCell {
    @IBOutlet weak var titleReviewLabel: UILabel!
    @IBOutlet weak var starratingView: AARatingBar!
    
    @IBOutlet weak var reviewerUserImageView: UIImageView!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var reviewerNameLabel: UILabel!
    @IBOutlet weak var ratingImageImageView: UIView!
    @IBOutlet weak var ratingLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
