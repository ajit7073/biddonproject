//
//  BrowseViewController.swift
//  bop
//
//  Created by apple on 21/02/19.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit


class BrowseViewController: UIViewController {

    @IBOutlet weak var browseTableView: UITableView!
    var mySkillArray : [String] = ["bet365.com type software","data entery and web research -- 2"]
    var categoryArray = [[String:Any]]() //= ["Websites, IT & Software","Writing & Contact","Design,Media & Architecture"]
    var urlName = String()
    var workerDetailArr = [[String:Any]]()
    var allProjectArr = [[String:Any]]()
    override func viewDidLoad() {
        super.viewDidLoad()
        browseTableView.dataSource = self
        browseTableView.delegate = self
        browseTableView.register(UINib.init(nibName: "workerListTableViewCell", bundle: nil), forCellReuseIdentifier: "workerListTableViewCell")
        getCategories()
        workerDetails()
        getPostListFilterWise(type:"",urlAPI:"")

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tabBarController?.tabBar.isHidden = false
    }


    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        
        if segue.identifier == "rattingSegue" {
            let destination = segue.destination as! WebsiteViewController
            destination.urlName = urlName
            destination.ProjectTitle =  sender as? String ?? ""
        }else if segue.identifier == "segueUserDetails" {
            let userDetails = segue.destination as! UserDetailsViewController
            userDetails.setAlertDelegate = self
            userDetails.userDetails = sender as! [String : Any]
        }else if segue.identifier == "segueProjectDetails" {
            let openProjectDetailsDetination = segue.destination as! ProjectDetailsViewController
            openProjectDetailsDetination.workOpen = true
            openProjectDetailsDetination.setAlertDelegate = self
            openProjectDetailsDetination.project = sender as! [String : Any]
        }
    }
    

}

// MARK: - Browse TableView DataSource And Deleget

extension BrowseViewController:UITableViewDataSource,UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        if UserDefaults.standard.object(forKey: "i_want_to") as? String ?? "" == "Hire" {
            return 3
        }else {
            return 3
        }
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if UserDefaults.standard.object(forKey: "i_want_to") as? String ?? "" == "Hire" {
            if section == 0 {
                return workerDetailArr.count
            }else if section == 1 {
                return mySkillArray.count
            }else {
                 return categoryArray.count
            }
        }else {
            if section == 0 {
                return allProjectArr.count
            }else if section == 1 {
                return mySkillArray.count
            }else {
                return categoryArray.count
            }
        }
    }
    
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if UserDefaults.standard.object(forKey: "i_want_to") as? String ?? "" == "Hire" {
            if indexPath.section == 0 {
                 let identifierWorker = "workerListTableViewCell"
                 let cell = browseTableView.dequeueReusableCell(withIdentifier: identifierWorker, for: indexPath) as! workerListTableViewCell
                
                
                let arrayRate = [4.0,2.0,3.0]
                let value = workerDetailArr[indexPath.row]
                cell.rattingBarView.value = CGFloat(arrayRate[indexPath.row])
                cell.countryNameLabel.text = value["Worker_Name"] as? String ?? ""
                cell.reviewLabel.text = value["worker_review"] as? String ?? ""
                cell.priceLable.text = value["rate"] as? String ?? ""
                let url = URL(string: value["Worker_Image"] as? String ?? "")
                
                cell.workerImageView.sd_setImage(with: url, placeholderImage:nil, completed: { (image, error, cacheType, url) -> Void in
                    if (error == nil) {
                        // set the placeholder image here
                        
                    } else {
                        cell.workerImageView.image = image
                        // success ... use the image
                    }
                })
                
                return cell
                
            }else if indexPath.section == 1 {
                let identifier = "BrowseTableViewCell"
                let cell = browseTableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! BrowseTableViewCell
                    cell.namebrowselabel.text = mySkillArray[indexPath.row]
                cell.indicationImageView.isHidden = true
                 cell.selectionStyle = .none
                
                //            cell.valueBrowseLabel.text = ""
                return cell
                
            }else {
                let identifier = "BrowseTableViewCell"
                let cell = browseTableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! BrowseTableViewCell
                cell.indicationImageView.isHidden = false
                cell.viewHeightConstraints.constant = 0
                let value = categoryArray[indexPath.row]
                cell.namebrowselabel.text = value["Language_Name"] as? String ?? ""
                
                return cell
            }
        
            
        }else {
            let identifier = "BrowseTableViewCell"
            let cell = browseTableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! BrowseTableViewCell
            cell.indicationImageView.isHidden = false
            if indexPath.section == 0 {
                let values = allProjectArr[indexPath.row]
                cell.namebrowselabel.text = values["Project_Name"] as? String ?? ""
              //ell.valueBrowseLabel.text  = values["Project_Detail"] as? String ?? ""
            }else if indexPath.section == 1 {
                cell.namebrowselabel.text = mySkillArray[indexPath.row]
                cell.indicationImageView.isHidden = true
                cell.selectionStyle = .none
            
                //            cell.valueBrowseLabel.text = ""
                
            }else{
                cell.viewHeightConstraints.constant = 0
                let value = categoryArray[indexPath.row]
                cell.namebrowselabel.text = value["Language_Name"] as? String ?? ""
                //            cell.valueBrowseLabel.text = ""
                
            }
            return cell
        }
        
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if UserDefaults.standard.object(forKey: "i_want_to") as? String ?? "" == "Hire" {
            if indexPath.section == 0 {
                   return 120
            }else {
                return 80
            }
         
        }else {
             return 80
        }
       
        
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView:UIView = UIView()
        headerView.backgroundColor = UIColor.white
        let titleLabel = UILabel()
        let subTitle = UILabel()
//        if UserDefaults.standard.object(forKey: "i_want_to") as? String ?? "" == "Hire" {??
            if section == 0 {
                titleLabel.text = "Recommended"
                subTitle.text = "Recommended freelancers for you"
            }else if section == 1 {
                titleLabel.text = " My Skills"
                subTitle.text = "Browse project by your skills"
            }else {
                titleLabel.text = "Categories"
                subTitle.text = "Browse project by categories"
            }
//        }
       
        
        titleLabel.frame = CGRect(x: 10, y:0 , width: tableView.frame.size.width, height: 30)
        subTitle.frame = CGRect(x: 10, y:30 , width: tableView.frame.size.width, height: 30)
        titleLabel.textColor = UIColor.black
        titleLabel.font = UIFont.boldSystemFont(ofSize: 25)
        subTitle.textColor = UIColor.lightGray
        headerView.addSubview(titleLabel)
        headerView.addSubview(subTitle)
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 70
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footerView:UIView = UIView()
        let showMoreButton = UIButton()
        showMoreButton.setTitle("Show More", for: .normal)
        showMoreButton.frame = CGRect(x: 0 ,y: 0 , width: tableView.frame.size.width ,height: 30 )
        footerView.addSubview(showMoreButton)
        
        return footerView
        
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if UserDefaults.standard.object(forKey: "i_want_to") as? String ?? "" == "Hire" {
            if indexPath.section == 0 {
                let userDeatils = workerDetailArr[indexPath.row]
                self.performSegue(withIdentifier: "segueUserDetails", sender: userDeatils)
            }
        }else {
            if indexPath.section == 0 {
                let userDeatils = allProjectArr[indexPath.row]
                self.performSegue(withIdentifier: "segueProjectDetails", sender: userDeatils)
                
            }
            
        }
        if indexPath.section == 2{
            
            if indexPath.row == 0 {
                urlName = "select_website.php"
                
            }else if indexPath.row == 1 {
                
                urlName = "select_mobile.php"
            }else if indexPath.row == 2 {
                urlName = "select_art.php"
            }else if indexPath.row == 3 {
                urlName = "select_data_entery.php"
            }
            
            let categoryName = categoryArray[indexPath.row]
            performSegue(withIdentifier: "rattingSegue", sender:categoryName["Language_Name"] )
        }
        
        
    }
}



extension BrowseViewController {

    
    func alertMessage(message:String) {
        let alert = UIAlertController(title: "", message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "ok", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    
    func getPostListFilterWise(type:String,urlAPI:String){
        
        let request = NSMutableURLRequest(url: NSURL(string: "\(baseUrl)select_project.php")! as URL)
        request.httpMethod = "GET"
        //        let postString = "text=\(type)"
        //        print(postString)
        //        request.setValue("application/x-www-form-urlencoded; charset=utf-8", forHTTPHeaderField: "Content-Type")
        //        request.httpBody = postString.data(using: String.Encoding.utf8)
        
        let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
            guard error == nil && data != nil else {                                                          // check for fundamental networking error
                print("error=\(String(describing: error))")
                return
            }
            
            do {
                if let responseJSON = try JSONSerialization.jsonObject(with: data!) as? [String:AnyObject]{
                    print(responseJSON)
                    print(responseJSON["success"]!)
                    DispatchQueue.main.async {
                        self.allProjectArr = responseJSON["project_details"] as! [[String : Any]]
                        DispatchQueue.main.async {
                            self.browseTableView.reloadData()
                        }
                    }
                }
            }
            catch {
                print("Error -> \(error)")
            }
        }
        task.resume()
    }

    
    func workerDetails(){
        let request = NSMutableURLRequest(url: NSURL(string: "\(baseUrl)select_worker_details.php")! as URL)
        request.httpMethod = "POST"
        request.setValue("application/x-www-form-urlencoded; charset=utf-8", forHTTPHeaderField: "Content-Type")
        
        
        let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
            guard error == nil && data != nil else {                                                          // check for fundamental networking error
                print("error=\(String(describing: error))")
                return
            }
            
            do {
                if let responseJSON = try JSONSerialization.jsonObject(with: data!) as? [String:AnyObject]{
                    print(responseJSON)
                    print(responseJSON["success"]!)
                    
                    self.workerDetailArr = responseJSON["Worker_details"] as! [[String : Any]]
                    DispatchQueue.main.async {
                        self.browseTableView.reloadData()
                    }
                    
                }
            }
            catch {
                print("Error -> \(error)")
            }
            
            
            
        }
        
        
        task.resume()
        
        
        
    }
    
    
    func getCategories(){
        let request = NSMutableURLRequest(url: NSURL(string: "\(baseUrl)select_language.php")! as URL)
        request.httpMethod = "GET"
       
       
        request.setValue("application/x-www-form-urlencoded; charset=utf-8", forHTTPHeaderField: "Content-Type")
       
        
        let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
            guard error == nil && data != nil else {                                                          // check for fundamental networking error
                print("error=\(String(describing: error))")
                return
            }
            
            do {
                if let responseJSON = try JSONSerialization.jsonObject(with: data!) as? [String:AnyObject]{
                    print(responseJSON)
                    print(responseJSON["success"]!)
                    let language_details = responseJSON["language_details"]
                    self.categoryArray =  language_details as! [[String : Any]]
                    DispatchQueue.main.async {
                        self.browseTableView.reloadData()
                    }
                }
            }
            catch {
                print("Error -> \(error)")
            }
        }
        
        task.resume()
        
        
        
    }
}

extension BrowseViewController: UserDetailsViewControllerDelegate {
    func setalertMessage() {
        alertMessage(message: " You are hering for this project as soon as contact administators Person")
    }

}

extension BrowseViewController :ProjectDetailsViewControllerDelegate {
    func setAlertMessage() {
        alertMessage(message: "You are active in this project on biding")
    }
    
    
}
