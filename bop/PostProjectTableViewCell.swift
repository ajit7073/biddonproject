//
//  PostProjectTableViewCell.swift
//  bop
//
//  Created by apple on 22/02/19.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit

class PostProjectTableViewCell: UITableViewCell {

    @IBOutlet weak var seletedImageView: UIImageView!
    @IBOutlet weak var valuePostProjectLabel: UILabel!
    @IBOutlet weak var namePostProjectLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
