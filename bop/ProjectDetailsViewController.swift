//
//  ProjectDetailsViewController.swift
//  bop
//
//  Created by apple on 10/04/19.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit

protocol ProjectDetailsViewControllerDelegate {
    func setAlertMessage()
}

class ProjectDetailsViewController: UIViewController {
    
    @IBOutlet weak var projectTitleLable: UILabel!
    
    @IBOutlet weak var dayLeftLable: UILabel!
    
    @IBOutlet weak var discriptionProjectLable: UILabel!
    
    @IBOutlet weak var placeBidButton: UIButton!
    @IBOutlet weak var budgetValuesLable: UILabel!
    @IBOutlet weak var avgBidLabel: UILabel!
    @IBOutlet weak var bidLabel: UILabel!
    
    var project = [String:Any]()
    var workOpen = Bool()
    var projectBidsArray = [String]()
    var setAlertDelegate :ProjectDetailsViewControllerDelegate?
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tabBarController?.tabBar.isHidden = true
       
        
        // Do any additional setup after loading the view.
        if workOpen {
            projectTitleLable.text = project["Project_Name"] as? String ?? ""
            dayLeftLable.text = "OPEN /  " + "\(project["Day_Left"] as? String ?? "")" + " Days Left"
            
            discriptionProjectLable.text = project["Project_Detail"] as? String ?? ""
            budgetValuesLable.text = project["Project_Rate"] as? String ?? ""
            bidLabel.text = project["Bids"] as? String ?? ""
        }else {
            projectTitleLable.text = project["Pro_Name"] as? String ?? ""
            dayLeftLable.text = "OPEN /  " + "\(project["Day_Left"] as? String ?? "")" + " Days Left"
            
            discriptionProjectLable.text = project["Pro_Name"] as? String ?? ""
            budgetValuesLable.text = project["Project_Rate"] as? String ?? ""
            bidLabel.text = project["Bids"] as? String ?? ""
        }
        
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
//        tabBarController?.tabBar.isHidden = false
    }
    
    @IBAction func placeBidBUttonAction(_ sender: Any) {
       // UserDefaults.standard.set("\(String(describing: project["Pro_ID"]))", forKey: "bids")
        let value = bidLabel.text
        bidLabel.text =  "\(Int(value!)! + 1 )"
        alertMessage(message: "You are active in this project on biding")
//        self.navigationController?.popViewController(animated: true)
//        setAlertDelegate?.setAlertMessage()
        //placeBidButton.isHidden = true
        
    }
    //MARk:- Alert Message
    func alertMessage(message:String) {
        let alert = UIAlertController(title: "", message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "ok", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
