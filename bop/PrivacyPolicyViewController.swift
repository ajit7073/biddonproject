//
//  PrivacyPolicyViewController.swift
//  bop
//
//  Created by apple on 27/03/19.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit
import SVProgressHUD

class PrivacyPolicyViewController: UIViewController {

    @IBOutlet weak var policyWebView: UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        SVProgressHUD.show()
        let url = Bundle.main.url(forResource: "policy", withExtension: "html")
        let request = NSURLRequest(url: url!)
        self.policyWebView.loadRequest(request as URLRequest)
        SVProgressHUD.dismiss()
        
        

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
    
    
    
    
    
}

