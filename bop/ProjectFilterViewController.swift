//
//  ProjectFilterViewController.swift
//  bop
//
//  Created by apple on 04/03/19.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit
protocol ProjectFilterDeleget {
    func getProjectStatusFilterWise(type:String)
}
class ProjectFilterViewController: UIViewController {
    
    @IBOutlet var applyFilterButton: UIView!
    @IBOutlet weak var filterTableView: UITableView!
     var countRadio = [Int]()
    var accountType : [String] = ["Employer","Freelancer"]
    var freelancerprojectStatus : [String] = ["All Project","Active bids","Work in progress","Post Project"]
    var emploerProjectStatus : [String] = ["All Project","Open for Bidding","Work in progress","Post Project"]
    var projectStatus =  [String]()
    
    var accountSelectedIndex  = Int()
    var projectSelectedIndex  = Int()
    var deleget :ProjectFilterDeleget?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tabBarController?.tabBar.isHidden = true
        filterTableView.delegate = self
        filterTableView.dataSource = self
        projectStatus = emploerProjectStatus
        applyFilterButton.layer.cornerRadius = 5
       
        // Do any additional setup after loading the view.
    }
    
   
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
         tabBarController?.tabBar.isHidden = false
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    @IBAction func applyFilterButtonAction(_ sender: Any) {
        deleget?.getProjectStatusFilterWise(type:projectStatus[projectSelectedIndex] )
        self.navigationController?.popViewController(animated: true)
    }
    
}

// MARK: - Browse TableView DataSource And Deleget

extension ProjectFilterViewController:UITableViewDataSource,UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return accountType.count
        }else {
            return projectStatus.count
        }
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let identifier = "ProjectFilterTableViewCell"
        let cell = filterTableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! ProjectFilterTableViewCell
        if indexPath.section == 0 {
            if accountSelectedIndex == indexPath.row {
                cell.radioImageView.image = UIImage(named: "filterSelectedIcon")
            }else{
                cell.radioImageView.image = UIImage(named: "filterUnselectedIcon")
            }
             cell.filterTypeLabel.text = accountType[indexPath.row]
        }else {
            if projectSelectedIndex == indexPath.row {
                cell.radioImageView.image = UIImage(named: "filterSelectedIcon")
            }else{
                cell.radioImageView.image = UIImage(named: "filterUnselectedIcon")
            }
             cell.filterTypeLabel.text = projectStatus[indexPath.row]
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
        
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView:UIView = UIView()
        headerView.backgroundColor = UIColor.clear
        let titleLabel = UILabel()
        if section == 0 {
            titleLabel.text = " Account Type"
        }else  {
            titleLabel.text = "Project Status"
        }
        titleLabel.frame = CGRect(x: 20, y: 15 , width: tableView.frame.size.width, height: 30)
        titleLabel.textColor =  UIColor.white
        titleLabel.font = UIFont.boldSystemFont(ofSize: 20)
        headerView.addSubview(titleLabel)
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.section == 0 {
            if indexPath.row == 0 {
                 projectStatus =  emploerProjectStatus
            }else {
                 projectStatus =  freelancerprojectStatus
            }
            accountSelectedIndex = indexPath.row
            
            
        }else {
             projectSelectedIndex = indexPath.row
        }
        filterTableView.reloadData()
        
    }
}
