//
//  MyProjectViewController.swift
//  bop
//
//  Created by apple on 21/02/19.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit
import SVProgressHUD

class MyProjectViewController: UIViewController {
    @IBOutlet weak var myProjectTableView: UITableView!
    
    @IBOutlet weak var topView: UIView!
    
    @IBOutlet weak var filterButton: UIButton!
    @IBOutlet weak var filterImageView: UIImageView!
    @IBOutlet weak var projectNameLabel: UILabel!
    var filterProjectWiseArr = [[String:Any]]()
    override func viewDidLoad() {
        super.viewDidLoad()
        myProjectTableView.delegate = self
        myProjectTableView.dataSource = self
//        getProjectStatusFilterWise(type: "\(baseUrl)select_project.php")
        getPostListFilterWise(type: "", urlAPI: "\(baseUrl)select_project.php")

        // Do any additional setup after loading the view.
    }
    

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        if segue.identifier == "projectFilterSegue" {
            let destination = segue.destination as! ProjectFilterViewController
            destination.deleget = self
        }
    }
    

    @IBAction func filterButtonAction(_ sender: Any) {
        performSegue(withIdentifier: "projectFilterSegue", sender: nil)
        
    }
    
    
    func getPostListFilterWise(type:String,urlAPI:String){
        SVProgressHUD.show()
        let request = NSMutableURLRequest(url: NSURL(string: urlAPI)! as URL)
        request.httpMethod = "GET"
//        let postString = "text=\(type)"
//        print(postString)
//        request.setValue("application/x-www-form-urlencoded; charset=utf-8", forHTTPHeaderField: "Content-Type")
//        request.httpBody = postString.data(using: String.Encoding.utf8)
        
        let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
            guard error == nil && data != nil else {                                                          // check for fundamental networking error
                print("error=\(String(describing: error))")
                return
            }
            
            do {
                if let responseJSON = try JSONSerialization.jsonObject(with: data!) as? [String:AnyObject]{
                    print(responseJSON)
                    print(responseJSON["success"]!)
                    DispatchQueue.main.async {
                        self.filterProjectWiseArr = responseJSON["project_details"] as! [[String : Any]]
                        self.myProjectTableView.reloadData()
                        SVProgressHUD.dismiss()
                    }
                }
                
            }
            catch {
                print("Error -> \(error)")
                SVProgressHUD.dismiss()
            }
            
            
            
        }
        
        
        task.resume()
        
        
        
    }
    
}

// MARK:- Tableview And TableViewCell

extension MyProjectViewController:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filterProjectWiseArr.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let identifier = "MyProjectTableViewCell"
        let cell = myProjectTableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! MyProjectTableViewCell
        let values = filterProjectWiseArr[indexPath.row]
        cell.nameMyProjectLabel.text = values["Project_Name"] as? String ?? ""
        cell.valueMyProjectLabel.text  = values["Project_Detail"] as? String ?? ""
        cell.selectionStyle = .none
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90
    }
    
}

extension MyProjectViewController : ProjectFilterDeleget {
    func getProjectStatusFilterWise(type: String) {
        //["All Project","Active bids","Work in progress","Post Project"]
        if type == "All Project" {
            getPostListFilterWise(type: type, urlAPI:"\(baseUrl)select_project.php" )
        }else if type == "Active bids"{
            getPostListFilterWise(type: type, urlAPI:"\(baseUrl)select_active_bid.php" )
        }else if type == "Work in progress"{
            getPostListFilterWise(type: type, urlAPI:"\(baseUrl)select_pending_bid.php" )
        }else if type == "Post Project" {
            getPostListFilterWise(type: type, urlAPI:"\(baseUrl)select_aprove_project.php" )
        }else if type == "Open for Bidding" {
            getPostListFilterWise(type: type, urlAPI:"\(baseUrl)select_bid.php" )
        }
        
    }
    
   
    
}

