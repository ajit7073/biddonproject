//
//  RegistrationViewController.swift
//  bop
//
//  Created by apple on 13/02/19.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit
protocol RegistrationViewControllerDelegate {
    func openAlert(title:String)
}

class RegistrationViewController: UIViewController {

    @IBOutlet weak var hireTextField: UITextField!
    
    @IBOutlet weak var registrationButton: UIButton!
    @IBOutlet weak var hireView: UIView!
    
    @IBOutlet weak var mobileTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var userNameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    var workarray : [String] = ["Work","Hire"]
    var workpicker = UIPickerView()
    var setAlertDelegate :RegistrationViewControllerDelegate?
  
    override func viewDidLoad() {
        super.viewDidLoad()
        
        registrationButton.layer.cornerRadius = 10
        hireView.layer.borderWidth = 1
        hireView.layer.borderColor = UIColor.black.cgColor
        hireTextField.text = workarray[0]
        hireTextField.delegate = self
        hireTextField.tag = 1
        mobileTextField.tag = 2
        emailTextField.tag = 3
        userNameTextField.tag = 4
        passwordTextField.tag = 5
        // Do any additional setup after loading the view.
        
        hideKeyboard()
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

   
    @IBAction func registrationButtonAction(_ sender: Any) {
       checkvalidation()
        
    }
    @IBAction func loginButtonAction(_ sender: Any) {
    performSegue(withIdentifier: "segueRegistrationLogin", sender: nil)
    }
    func hideKeyboard()
    {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(
            target: self,
            action: #selector(dismissKeyboard))
        
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard()
    {
        workpicker.isHidden = true
        view.endEditing(true)
    }
    
    // MARK :- Validation
    func checkvalidation(){
        if hireTextField.text?.count ?? 0 > 0{
            
        }else{
            alertMessage(message: "Select any one field")
            return
        }
        if  userNameTextField.text?.count ?? 0  > 0 {
            
        }else{
            alertMessage(message: "Enter Valid User Name")
            return
        }
        if emailTextField.text?.count ?? 0 > 0 {
            if validateEmail(enteredEmail: emailTextField.text ?? ""){
                
            }else {
                alertMessage(message: "Please valid email Address.")
                return
            }
        }else {
            alertMessage(message: " Required email address!")
            return
        }
        if  isValidated(passwordTextField.text ?? "") {
            
            
        }else {
            alertMessage(message: " Required password should be 8 character or must be strong.")
            return
        }
        if mobileTextField.text?.count == 10{
            
        }else{
            alertMessage(message: "Mobile number must be 10 digit!")
            return
        }
      
        
    postRegistrationCall()
    }
   
    func validateEmail(enteredEmail:String) -> Bool {
        let emailFormat = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailPredicate = NSPredicate(format:"SELF MATCHES %@", emailFormat)
        let email = emailPredicate.evaluate(with: enteredEmail)
        
        return email
    }
    func isValidated(_ password: String) -> Bool {
        var lowerCaseLetter: Bool = false
        var upperCaseLetter: Bool = false
        var digit: Bool = false
        var specialCharacter: Bool = false
        
        if password.characters.count  >= 8 {
            for char in password.unicodeScalars {
                if !lowerCaseLetter {
                    lowerCaseLetter = CharacterSet.lowercaseLetters.contains(char)
                }
                if !upperCaseLetter {
                    upperCaseLetter = CharacterSet.uppercaseLetters.contains(char)
                }
                if !digit {
                    digit = CharacterSet.decimalDigits.contains(char)
                }
                if !specialCharacter {
                    specialCharacter = CharacterSet.punctuationCharacters.contains(char)
                }
            }
            if specialCharacter || (digit && lowerCaseLetter && upperCaseLetter) {
                //do what u want
                return true
            }
            else {
                return false
            }
        }
        return false
    }
   
    
    func alertMessage(message:String) {
        let alert = UIAlertController(title: "", message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "ok", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
 
}

//MARK:-UITextFieldDelegate
extension RegistrationViewController:UITextFieldDelegate{
   
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField.tag == 1 {
            workpicker.isHidden = false
            workpicker.frame = CGRect(x: hireView.frame.origin.x, y: hireView.frame.origin.y + 30, width: hireView.frame.size.width, height: 100)
            workpicker.backgroundColor = UIColor.gray
            workpicker.layer.borderColor = UIColor.white.cgColor
            workpicker.layer.borderWidth = 1
            workpicker.delegate = self
            workpicker.dataSource = self
            hireTextField.inputView = workpicker
//            self.view.addSubview(workpicker)
            return true
        }else {
             self.workpicker.removeFromSuperview()
            return false
            
        }
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        return false
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        
        return true
    }
}

//MARK:-UIPickerViewDeleget And DataSource

extension RegistrationViewController: UIPickerViewDelegate,UIPickerViewDataSource
{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1 //workarray.count
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return workarray.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return workarray[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let value = workarray[row]
        hireTextField.text = value
        pickerView.isHidden = true
        workpicker.isHidden = true
    }
}

extension RegistrationViewController {
    
    // MARK:- API ALERT MESSAGE FUNCTION
    

    //MARK:- API
    
    func postRegistrationCall(){
        let request = NSMutableURLRequest(url: NSURL(string: "\(baseUrl)user_side.php")! as URL)
        request.httpMethod = "POST"
        let postString = "i_want_to=\(hireTextField.text!)&username=\(userNameTextField.text!)&email=\(emailTextField.text!)&pass=\(passwordTextField.text!)&mobile=\(mobileTextField.text!)"
        print(postString)
        request.setValue("application/x-www-form-urlencoded; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.httpBody = postString.data(using: String.Encoding.utf8)
        
        let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
            guard error == nil && data != nil else {                                                          // check for fundamental networking error
                print("error=\(String(describing: error))")
                return
            }
            
            do {
                if let responseJSON = try JSONSerialization.jsonObject(with: data!) as? [String:AnyObject]{
                    print(responseJSON)
                    print(responseJSON["success"]!)
                    
//                    //Check response from the sever
//                    let responds = responseJSON["success"]! as! Int
                    DispatchQueue.main.async {
//
//                        if responds == 1 {
//                            self.performSegue(withIdentifier: "segueTabbar", sender: nil)
                        self.userNameTextField.text = ""
                        self.emailTextField.text = ""
                        self.passwordTextField.text = ""
                        self.mobileTextField.text = ""
                        self.alertMessage(message: "Registration successfull!")
//                          self.performSegue(withIdentifier: "segueRegistrationLogin", sender: nil)
//                        DispatchQueue.main.async {
//                             self.setAlertDelegate?.openAlert(title: "Registration successfull!")
//                        }
                       
                        
                            //                         self.performSegue(withIdentifier: "segueLogin", sender: nil)
//                        }else {
//                            self.alertMessage(message:"Please Enter valid Your Data!")
//                        }
                    }
                    
                }
            }
            catch {
                print("Error -> \(error)")
            }
            
            
            
        }
        
        
        task.resume()
        
        
        
    }

}


