//
//  ViewController.swift
//  bop
//
//  Created by apple on 13/02/19.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit
import FSPagerView

class ViewController: UIViewController,FSPagerViewDelegate,FSPagerViewDataSource {
    
    @IBOutlet weak var pageView: FSPagerView!
    
    @IBOutlet weak var sliderCollectionView: UICollectionView!
    @IBOutlet weak var createAccountButton: UIButton!
    @IBOutlet weak var loginButton: UIButton!
    
    var imgArr = [ "one",  "two", "three", "four", "five","six"] // this is files in assets
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        setupUI()
        setSliderBarImage()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        
        if segue.identifier == "segueRegistration" {
            let destination = segue.destination as! RegistrationViewController
            destination.setAlertDelegate = self
        }
    }
    
    func setupUI() {
        if UserDefaults.standard.bool(forKey: "isLogin") {
            DispatchQueue.main.async {
                self.performSegue(withIdentifier: "openTabbarSegue", sender: nil)
            }
            
        }
        let attributes = [NSAttributedString.Key.underlineStyle: NSUnderlineStyle.single.rawValue]
        let attributedText = NSAttributedString(string: loginButton.currentTitle!, attributes: attributes)
        
        loginButton.titleLabel?.attributedText = attributedText
        
        
        createAccountButton.clipsToBounds = true
        createAccountButton.layer.cornerRadius = 10
        sliderCollectionView.delegate = self
        sliderCollectionView.dataSource = self
        
        
    }
    func setSliderBarImage() {
        pageView.dataSource = self
        pageView.delegate = self
        pageView.itemSize = CGSize(width: 200, height: 180)
        pageView.automaticSlidingInterval = 3.0
        pageView.interitemSpacing = 10
        pageView.isInfinite = true
        pageView.transformer = FSPagerViewTransformer(type: .overlap)
        pageView.register(FSPagerViewCell.self, forCellWithReuseIdentifier: "cell")
    }
    
    public func numberOfItems(in pagerView: FSPagerView) -> Int {
        return imgArr.count
    }
    
    public func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell {
        let cell = pagerView.dequeueReusableCell(withReuseIdentifier: "cell", at: index)
        cell.imageView?.image = UIImage(named:imgArr[index])
        cell.imageView?.clipsToBounds = true
        cell.imageView?.layer.cornerRadius  = 10
        
        return cell
    }
    
    @IBAction func createAccountButtonAction(_ sender: Any) {
        performSegue(withIdentifier: "segueRegistration", sender: nil)
    }
    @IBAction func loginButtonAction(_ sender: Any) {
        performSegue(withIdentifier: "segueLogin", sender: nil)
    }
    
    func alertMessage(message:String) {
        let alert = UIAlertController(title: "", message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "ok", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    
}


extension ViewController:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imgArr.count
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "sliderCollectionViewCell", for: indexPath) as! sliderCollectionViewCell
        cell.sliderImageView.image = UIImage(named: imgArr[indexPath.row])
        cell.pageControl.currentPage = indexPath.row
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        
        return CGSize(width:375, height:217)
    }
    
    // cell.cashOrCredit.image = UIImage(named: "1")
    
}

extension ViewController:RegistrationViewControllerDelegate {
    func openAlert(title: String) {
        alertMessage(message: title)
    }
    
    
}



