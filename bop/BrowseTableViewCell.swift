//
//  BrowseTableViewCell.swift
//  bop
//
//  Created by apple on 22/02/19.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit

class BrowseTableViewCell: UITableViewCell {
    @IBOutlet weak var namebrowselabel: UILabel!
    @IBOutlet weak var valueBrowseLabel: UILabel!
    
    @IBOutlet weak var indicationImageView: UIImageView!
    @IBOutlet weak var viewHeightConstraints: NSLayoutConstraint!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
