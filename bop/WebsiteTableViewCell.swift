//
//  WebsiteTableViewCell.swift
//  bop
//
//  Created by apple on 13/03/19.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit

class WebsiteTableViewCell: UITableViewCell {

    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var countryImageView: UIImageView!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var reviewsLable: UILabel!
    @IBOutlet weak var hourlyRateLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
