//
//  TermsViewController.swift
//  bop
//
//  Created by apple on 05/04/19.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit
import SVProgressHUD


class TermsViewController: UIViewController {

    @IBOutlet weak var termsWebView: UIWebView!
    override func viewDidLoad() {
        super.viewDidLoad()
        SVProgressHUD.show()
        let url = Bundle.main.url(forResource: "terms", withExtension: "html")
        let request = NSURLRequest(url: url!)
        self.termsWebView.loadRequest(request as URLRequest)
        SVProgressHUD.dismiss()

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
