//
//  WebsiteViewController.swift
//  bop
//
//  Created by apple on 13/03/19.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit
import SDWebImage
import SVProgressHUD

class WebsiteViewController: UIViewController {
    
    @IBOutlet weak var projectSegmentControl: UISegmentedControl!
    @IBOutlet weak var websiteTableview: UITableView!
    var workerDetailArr = [[String:Any]]()
    var projectList = [[String:Any]]()
    var urlName = String()
    var ProjectTitle  = String()
    override func viewDidLoad() {
        super.viewDidLoad()
        tabBarController?.tabBar.isHidden = true
        title = ProjectTitle
        websiteTableview.dataSource = self
        websiteTableview.delegate = self
        websiteTableview.register(UINib(nibName: "ProjectListTableViewCell", bundle: nil), forCellReuseIdentifier: "ProjectListTableViewCell")
        workerDetails()
        projectListApi(urlName: urlName)
        
        // Do any additional setup after loading the view.
    }
    
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
//        tabBarController?.tabBar.isHidden = false
    }
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
        if segue.identifier == "SegueProjectDetails" {
            let destination = segue.destination as! ProjectDetailsViewController
            destination.project = sender as! [String : Any]
        }else if segue.identifier == "segueUserDetails" {
            let destinationUser = segue.destination as! UserDetailsViewController
            destinationUser.userDetails = sender as! [String : Any]
        }
     }
    @IBAction func projectControlAction(_ sender: Any) {
        websiteTableview.reloadData()
    }
}
extension WebsiteViewController : UITableViewDataSource,UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if projectSegmentControl.selectedSegmentIndex == 0 {
            return projectList.count
        }else{
            return workerDetailArr.count
        }
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if projectSegmentControl.selectedSegmentIndex == 1 {
            let identifier = "WebsiteTableViewCell"
            let cell = websiteTableview.dequeueReusableCell(withIdentifier: identifier, for:indexPath) as! WebsiteTableViewCell
            let values = workerDetailArr[indexPath.row]
            cell.userNameLabel.text = values["Worker_Name"] as? String ?? ""
            let url = URL(string: values["Worker_Image"] as? String ?? "")
            cell.reviewsLable.text = values["worker_review"] as? String ?? ""
            cell.hourlyRateLabel.text = values["rate"] as? String ?? ""
            
            cell.userImageView.sd_setImage(with: url, placeholderImage:nil, completed: { (image, error, cacheType, url) -> Void in
                if (error == nil) {
                    // set the placeholder image here
                    
                } else {
                    cell.userImageView.image = image
                    // success ... use the image
                }
            })
            
            
            return cell
        }else {
            let identifier = "ProjectListTableViewCell"
            let cell = websiteTableview.dequeueReusableCell(withIdentifier: identifier, for:indexPath) as! ProjectListTableViewCell
            let values = projectList[indexPath.row]
            cell.projectName.text = values["Pro_Name"]  as? String ?? ""
            cell.projectValues.text = "\(values["Pro_Price"] as? String ?? "")"
            
            return cell
            
        }
       
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if projectSegmentControl.selectedSegmentIndex == 1 {
            return 90
        }else {
            return 70
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if projectSegmentControl.selectedSegmentIndex == 0 {
            let values = projectList[indexPath.row]
             self.performSegue(withIdentifier: "SegueProjectDetails", sender: values)
        }else {
            let userDeatils = workerDetailArr[indexPath.row]
            self.performSegue(withIdentifier: "segueUserDetails", sender: userDeatils)
        }
       
    }
}

extension WebsiteViewController {
    
    // MARK:- API ALERT MESSAGE FUNCTION
    
    
    //MARK:- API
    
    func workerDetails(){
        
        let request = NSMutableURLRequest(url: NSURL(string: "\(baseUrl)select_worker_details.php")! as URL)
        request.httpMethod = "POST"
        request.setValue("application/x-www-form-urlencoded; charset=utf-8", forHTTPHeaderField: "Content-Type")
        
        SVProgressHUD.show()
        let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
            guard error == nil && data != nil else {                                                          // check for fundamental networking error
                print("error=\(String(describing: error))")
                return
            }
            
            do {
                if let responseJSON = try JSONSerialization.jsonObject(with: data!) as? [String:AnyObject]{
                    print(responseJSON)
                    print(responseJSON["success"]!)
                     SVProgressHUD.dismiss()
                    self.workerDetailArr = responseJSON["Worker_details"] as! [[String : Any]]
                    DispatchQueue.main.async {
                        self.websiteTableview.reloadData()
                    }
                    
                }
            }
            catch {
                SVProgressHUD.dismiss()
                print("Error -> \(error)")
            }
        }
        
        
        task.resume()
        
    }
    
    func projectListApi(urlName:String){
        
        let request = NSMutableURLRequest(url: NSURL(string: "\(baseUrl)\(urlName)")! as URL)
        request.httpMethod = "GET"
        request.setValue("application/x-www-form-urlencoded; charset=utf-8", forHTTPHeaderField: "Content-Type")
        
        SVProgressHUD.show()
        let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
            guard error == nil && data != nil else {                                                          // check for fundamental networking error
                print("error=\(String(describing: error))")
                return
            }
            
            do {
                if let responseJSON = try JSONSerialization.jsonObject(with: data!) as? [String:AnyObject]{
                    SVProgressHUD.dismiss()
                    print(responseJSON)
                    print(responseJSON["success"]!)
                    self.projectList = responseJSON["projects"] as! [[String : Any]]
                    DispatchQueue.main.async {
                        self.websiteTableview.reloadData()
                    }
                    
                }
            }
            catch {
                SVProgressHUD.dismiss()
                print("Error -> \(error)")
            }
        }
        
        
        task.resume()
        
        
        
    }
}
