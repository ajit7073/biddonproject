//
//  AccountDetailsTableViewCell.swift
//  bop
//
//  Created by apple on 01/03/19.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit

class AccountDetailsTableViewCell: UITableViewCell {
    @IBOutlet weak var indicatorImage: UIImageView!
    @IBOutlet weak var profileNameLabel: UILabel!
    @IBOutlet weak var profileDetailsLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
