//
//  AccountViewController.swift
//  bop
//
//  Created by apple on 21/02/19.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit
import MessageUI

class AccountViewController: UIViewController {
    
    @IBOutlet weak var accountTableView: UITableView!
    var  notiArr : [String] = ["Post a Project"]
    var nitiDetailArr : [String] = ["It's free to post a project!"]
    var  accoArr : [String] = ["Membership","Payments/Deposite funds"]
    var accoDetailArr : [String] = ["Upgrade your membership for a better value","Upgrade your membership for a better value"]
    var  notifiArr : [String] = ["New Message","Vibration"]
    var notifiDetailArr : [String] = ["Show notification for new incoming message","Show notification for new incoming message"]
    var  aboutArr : [String] = ["App Version","rating"]
    var aboutDetailArr : [String] = ["3.6.31(1903060396)","3.6.31(1903060396)","3.6.31(1903060396)"]
    var  otherArr : [String] = ["Privacy Policy","Terms and Conditions","Contact Support","Articles and How-to's"]
    var otherDetailArr : [String] = ["Opens our privacy policy","Opens your Terms and Conditions ","Our awesome Customer Support Team is readt to assist you"]
    var countCheckBox = [Int]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        accountTableView.dataSource = self
        accountTableView.delegate = self
//        self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: UserDefaults.standard.string(forKey: "username") ?? "", style: UIBarButtonItem.Style.plain, target: self, action: nil)
        let rightBarButtonItem = UIBarButtonItem.init(image: UIImage(named: "logoutImage"), style: .done, target: self, action: #selector(logputButton))
        
        self.navigationItem.rightBarButtonItem = rightBarButtonItem
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    @objc func logputButton() {
        
        DispatchQueue.main.async {
            UserDefaults.standard.set(false, forKey: "isLogin")
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = mainStoryboard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
            UIApplication.shared.keyWindow?.rootViewController = vc
        }
        
    }
    
}

// MARK:- Account DataSource And Deleget

extension AccountViewController:UITableViewDataSource,UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 4
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0{
            return 1
        }else if section == 1 {
            return notiArr.count
        }else if section == 2 {
             return aboutArr.count//accoArr.count
        }else {
                return  otherDetailArr.count//notifiArr.count
        }
//        else if section == 4 {
//            return aboutArr.count
//        }else {
//            return otherDetailArr.count
//        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
        let identifier = "AccountTableViewCell"
            let cell = accountTableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! AccountTableViewCell
            cell.nameAccountLabel.text = "Available Balance"
            cell.valueAccountLabel.text = "$0.00USD"
            return cell
            
        }else{
          let identifire2 = "AccountDetailsTableViewCell"
            let cell = accountTableView.dequeueReusableCell(withIdentifier: identifire2, for: indexPath) as! AccountDetailsTableViewCell
            if indexPath.section == 1 {
                cell.indicatorImage.image = UIImage(named: "indicatorIcon")
                cell.indicatorImage?.isHidden = false
                cell.profileDetailsLabel.text = nitiDetailArr[indexPath.row]
                cell.profileNameLabel.text = notiArr[indexPath.row]
            }else if indexPath.section == 2{
                if indexPath.row == 0 {
                    cell.indicatorImage?.isHidden = true
                    cell.profileDetailsLabel.text = version()
                    cell.profileNameLabel.text = aboutArr[indexPath.row]
                    cell.selectionStyle = .none
                }else {
//                    cell.indicatorImage.image = UIImage(named: "indicatorIcon")
                    cell.indicatorImage?.isHidden = true
                    cell.profileDetailsLabel.text = aboutDetailArr[indexPath.row]
                    cell.profileNameLabel.text = aboutArr[indexPath.row]
                    cell.selectionStyle = .none
                }
//                cell.indicatorImage.image = UIImage(named: "indicatorIcon")
//                cell.indicatorImage?.isHidden = false
//                cell.profileDetailsLabel.text = accoDetailArr[indexPath.row]
//                cell.profileNameLabel.text = accoArr[indexPath.row]
            }else{
                cell.indicatorImage?.isHidden = true
                cell.profileDetailsLabel.text = otherDetailArr[indexPath.row]
                cell.profileNameLabel.text = otherArr[indexPath.row]
            }//else if indexPath.section == 3 {
//                cell.profileDetailsLabel.text = notifiDetailArr[indexPath.row]
//                cell.profileNameLabel.text = notifiArr[indexPath.row]
//                if countCheckBox.count > 0 {
//                    if countCheckBox.contains(indexPath.row){
//                        cell.indicatorImage.image = UIImage(named: "checkBoxSelectedIcon")
//                    }else {
//                       cell.indicatorImage.image = UIImage(named: "checkBoxIcon")
//                    }
//
//                }else {
//                    cell.indicatorImage.image = UIImage(named: "checkBoxIcon")
//                }
//
//            }else if indexPath.section == 4{
//
//
//
//            }
            
            return cell
        }
 
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return 140
        }
        return 80
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 3 {
            if indexPath.row == 0 {
                self.performSegue(withIdentifier: "policySegue", sender: nil)
            }else if indexPath.row == 1 {
                self.performSegue(withIdentifier: "termsSegue", sender: nil)
            }else if indexPath.row == 2 {
                openMailComposer()
            }
        }else if indexPath.section == 1 {
            if indexPath.row == 0 {
                tabBarController?.selectedIndex = 1
            }
        }
        
    }
    
   
     func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView:UIView = UIView()
        headerView.backgroundColor = UIColor.lightGray
        let titleLabel = UILabel()
        if section == 0 {
            titleLabel.text = " Notification"
        }else if section == 1 {
            titleLabel.text = ""
        }else if section == 2 {
            titleLabel.text = " About"
        }else if section == 3 {
            titleLabel.text = " Other"
        }
        titleLabel.frame = CGRect(x: 0, y:0 , width: tableView.frame.size.width, height: 30)
        titleLabel.textColor = UIColor.init(red: 98/255, green: 167/255, blue: 248/255, alpha: 1)
        headerView.addSubview(titleLabel)
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 1 {
            return 0
        }
        return 30
    }
}

extension AccountViewController {
    
    func version() -> String {
        let dictionary = Bundle.main.infoDictionary!
        let version = dictionary["CFBundleShortVersionString"] as! String
        let build = dictionary["CFBundleVersion"] as! String
        return "\(version) (\(build))"
    }
    
   
}

extension AccountViewController : MFMailComposeViewControllerDelegate {
    
    func openMailComposer() {
        
        if !MFMailComposeViewController.canSendMail() {
            print("Mail services are not available")
            return
        }
        let composeVC = MFMailComposeViewController()
        composeVC.mailComposeDelegate = self
        
        // Configure the fields of the interface.
        composeVC.setToRecipients(["parthbhagat5223@gmail.com"])
        composeVC.setSubject("Hello!")
        composeVC.setMessageBody("", isHTML: false)
        
        // Present the view controller modally.
        self.present(composeVC, animated: true, completion: nil)
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        self.dismiss(animated: true, completion: nil)
        
        
    }
}
