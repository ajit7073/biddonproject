//
//  AccountTableViewCell.swift
//  bop
//
//  Created by apple on 22/02/19.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit

class AccountTableViewCell: UITableViewCell {
    @IBOutlet weak var borderView: UIView!
    @IBOutlet weak var nameAccountLabel: UILabel!
    @IBOutlet weak var valueAccountLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.borderView.layer.borderColor = UIColor.blue.cgColor
        self.borderView.layer.borderWidth = 1
        self.borderView.clipsToBounds = true
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
