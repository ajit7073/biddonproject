//
//  ProjectListTableViewCell.swift
//  bop
//
//  Created by apple on 02/04/19.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit

class ProjectListTableViewCell: UITableViewCell {

   
    @IBOutlet weak var projectName: UILabel!
    
    @IBOutlet weak var projectValues: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
