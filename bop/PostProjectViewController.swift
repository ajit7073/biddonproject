//
//  PostProjectViewController.swift
//  bop
//
//  Created by apple on 21/02/19.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit

class PostProjectViewController: UIViewController {
    @IBOutlet weak var hourlyRateTextField: UITextField!
    @IBOutlet weak var segmentControl: UISegmentedControl!
    @IBOutlet weak var titleTextField: UITextField!
    @IBOutlet weak var discriptionTextField: UITextField!
    @IBOutlet weak var moneyPickerTextField: UITextField!
    @IBOutlet weak var countryView: UIView!
    @IBOutlet weak var moneyView: UIView!
    @IBOutlet weak var postProjectTableView: UITableView!
    @IBOutlet weak var addAttechmentButton: UIButton!
    @IBOutlet weak var countryPickerTextField: UITextField!
    
    @IBOutlet weak var selectedImageView: UIImageView!
    
    var countCheckBox = [Int]()
    var countryarray : [String] = ["EUR","GBP","INR","SGD","USD"]
    var countrypicker = UIPickerView()
    var moneyarray : [String] = ["500","1000","1500","2000","2500+"]
    var hourlyarray : [String] = ["$ 100-200/hr","$ 400-750/hr","$ 1250-2500/hr","$ 2500+/hr"]
    var moneypicker = UIPickerView()
    var pickerArray = [String]()
    var imagePicker: UIImagePickerController!
    var ImageFileUpload = UIImage()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        postProjectTableView.dataSource = self
        postProjectTableView.delegate = self
        countryPickerTextField.delegate = self
        moneyPickerTextField.delegate = self
        hourlyRateTextField.delegate = self
        countryPickerTextField.tag = 1
        moneyPickerTextField.tag = 2
        hourlyRateTextField.tag = 3
        moneyPickerTextField.text = moneyarray[0]
        countryPickerTextField.text = countryarray[0]
        hourlyRateTextField.text = hourlyarray[0]
        
        hideKeyboard()
        // Do any additional setup after loading the view.
    }
    func openPickerView(tag:Int) {
        countrypicker.isHidden = false
        countrypicker.backgroundColor = UIColor.gray
        countrypicker.layer.borderColor = UIColor.white.cgColor
        countrypicker.layer.borderWidth = 1
        countrypicker.delegate = self
        countrypicker.dataSource = self
        countrypicker.selectRow(0, inComponent: 0, animated: false)
        if tag == 1{
//            moneyPickerTextField.isHidden = true
            countryPickerTextField.inputView = countrypicker
        }else if tag == 2{
//            countryPickerTextField.isHidden = true
            moneyPickerTextField.inputView = countrypicker
        }else if tag == 3{
            hourlyRateTextField.inputView = countrypicker
        }
        
//        self.view.addSubview(countrypicker)
        
    }
    func hideKeyboard()
    {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(
            target: self,
            action: #selector(dismissKeyboard))
        
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard()
    {
        countrypicker.isHidden = true
        moneypicker.isHidden = true
        view.endEditing(true)
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     
     */
    
    //MARK:- UIButton Action
    
    @IBAction func hourlySegmentsControlsAction(_ sender: Any) {
        
        if segmentControl.selectedSegmentIndex == 0 {
            hourlyRateTextField.isHidden = true
            countryView.isHidden = false
            moneyView.isHidden =  false
            
        }else {
            countryView.isHidden = true
            moneyView.isHidden =  true
            hourlyRateTextField.isHidden = false
            
        }
    }
    @IBAction func postProjectButtonAction(_ sender: Any) {
        if isValidate() {
            postProjectApi()
        }
        
    }
    @IBAction func addAttechmentButtonAction(_ sender: Any) {
        
        imagePicker =  UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = .photoLibrary
        present(imagePicker, animated: true, completion: nil)
    }
    func postProjectApi(){
        let request = NSMutableURLRequest(url: NSURL(string:"\(baseUrl)insert_post_project.php")! as URL)
        request.httpMethod = "POST"
        let postString = "Project_Name=\(titleTextField.text ?? "")&Project_Detail=\(discriptionTextField.text ?? "")&Project_Rate=\(moneyPickerTextField.text ?? "")&Images=\(String(describing: ImageFileUpload.images))"
        print(postString)
        request.setValue("application/x-www-form-urlencoded; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.httpBody = postString.data(using: String.Encoding.utf8)
        
        let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
            guard error == nil && data != nil else {                                                          // check for fundamental networking error
                print("error=\(String(describing: error))")
                return
            }
            do {
                if let responseJSON = try JSONSerialization.jsonObject(with: data!) as? [String:Any]{
                    print(responseJSON)
                    print(responseJSON["success"]!)
                    
                    
                    //                    //Check response from the sever
                    let responds = responseJSON["success"]! as! Int
                    DispatchQueue.main.async {
                        
                        if responds == 1 {
                            self.titleTextField.text = ""
                            self.discriptionTextField.text = ""
                            self.moneyPickerTextField.text = ""
                            self.countryPickerTextField.text = ""
                            self.hourlyRateTextField.text = ""
                            self.selectedImageView.image = UIImage(named: "")
                            
                            self.alertMessage(message:"Your post upload successfull.")
                        }else {
                            self.alertMessage(message:"Please Enter valid Your Data!")
                        }
                    }
                    
                }
            }
            catch {
                print("Error -> \(error)")
            }
        }
        
        task.resume()
    }
    
    
    func alertMessage(message:String) {
        let alert = UIAlertController(title: "", message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "ok", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func isValidate()-> Bool {
        
        if titleTextField.text?.count ?? 0 <= 0 {
            alertMessage(message: "Please input valid project title. ")
            return false
        }else if discriptionTextField.text?.count ?? 0 <= 0 {
             alertMessage(message: "Please input valid project descripation. ")
            return false
        }else if segmentControl.selectedSegmentIndex == 0 {
            if moneyPickerTextField.text?.count ?? 0  <= 0 {
                 alertMessage(message: "Please choose buget price. ")
                return false
            }else if countryPickerTextField.text?.count ?? 0 <= 0  {
                 alertMessage(message: "Please choose country currency code. ")
                return false
            }
        }else if segmentControl.selectedSegmentIndex == 1 {
            if hourlyRateTextField.text?.count ?? 0 <= 0 {
                 alertMessage(message: "Please choose hourly rate. ")
                return false
            }
        }
        return true
    }
}

// MARK :- PostProject Dtasource And Deleget

extension PostProjectViewController:UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let identifier = "PostProjectTableViewCell"
        let cell = postProjectTableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! PostProjectTableViewCell
        if countCheckBox.count > 0 {
            if countCheckBox.contains(indexPath.row){
                cell.seletedImageView.image = UIImage(named: "checkBoxSelectedIcon")
            }else {
                cell.seletedImageView.image = UIImage(named: "checkBoxIcon")
            }
            
        }else {
            cell.seletedImageView.image = UIImage(named: "checkBoxIcon")
        }
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if countCheckBox.contains(indexPath.row){
            countCheckBox.remove(at: indexPath.row)
        }else {
            countCheckBox.append(indexPath.row)
        }
        postProjectTableView.reloadData()
        
        
    }
}

//MARK:-UITextFieldDelegate
extension PostProjectViewController:UITextFieldDelegate{
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField.tag == 1 {
            pickerArray = countryarray
            countrypicker.frame = CGRect(x: countryView.frame.origin.x, y: countryView.frame.origin.y + 30, width: countryView.frame.size.width, height: 200)
            countrypicker.tag = 1
            openPickerView(tag: 1)
            return true
        }else if textField.tag == 2 {
            pickerArray = moneyarray
            countrypicker.frame = CGRect(x: moneyView.frame.origin.x, y: moneyView.frame.origin.y + 30, width: moneyView.frame.size.width, height: 200)
            countrypicker.tag = 2
            openPickerView(tag: 2)
            return true
            
        }else if textField.tag == 3 {
            pickerArray = hourlyarray
            countrypicker.frame = CGRect(x: hourlyRateTextField.frame.origin.x, y: hourlyRateTextField.frame.origin.y + 30, width: hourlyRateTextField.frame.size.width, height: 200)
            countrypicker.tag = 3
            openPickerView(tag: 3)
            return true
        }else {
            self.countrypicker.removeFromSuperview()
            return false
        }
        
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        return false
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        
        return true
    }
}

//MARK:-UIPickerViewDeleget And DataSource

extension PostProjectViewController: UIPickerViewDelegate,UIPickerViewDataSource
{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1 //workarray.count
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerArray.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        return pickerArray[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let value = pickerArray[row]
        if pickerView.tag == 1{
            countryPickerTextField.text = value
             countryPickerTextField.resignFirstResponder()
        }else if pickerView.tag == 2{
            moneyPickerTextField.text = value
            moneyPickerTextField.resignFirstResponder()
        }else {
            hourlyRateTextField.text = value
            hourlyRateTextField.resignFirstResponder()
        }
        pickerView.isHidden = true
       
        
    }
}

extension PostProjectViewController :UINavigationControllerDelegate,UIImagePickerControllerDelegate {
    
    //MARK: - Done image capture here
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]){
        
        guard let selectedImage = info[.originalImage] as? UIImage else {
            print("Image not found!")
            return
        }
        ImageFileUpload = selectedImage
        selectedImageView.image = selectedImage
        
        picker.dismiss(animated: true, completion: nil)
    }
    
}
