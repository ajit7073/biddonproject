//
//  ProjectFilterTableViewCell.swift
//  bop
//
//  Created by apple on 04/03/19.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit

class ProjectFilterTableViewCell: UITableViewCell {
    @IBOutlet weak var filterTypeLabel: UILabel!
    @IBOutlet weak var radioImageView: UIImageView!
    
    @IBOutlet weak var filterSeletedImageView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
