//
//  MyProjectTableViewCell.swift
//  bop
//
//  Created by apple on 22/02/19.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit

class MyProjectTableViewCell: UITableViewCell {

    @IBOutlet weak var valueMyProjectLabel: UILabel!
    @IBOutlet weak var nameMyProjectLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
